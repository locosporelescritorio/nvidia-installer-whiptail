TERM=vt220
dist=$(tr -s ' \011' '\012' < /etc/issue | head -n 1)
check_arch=$(uname -m)
distL=$(echo "$dist" | tr '[:upper:]' '[:lower:]')
# distL='ubuntu'
distro1='debian'
distro2='ubuntu'

if [ "$(whoami)" != "root" ]; then
    whiptail --msgbox "Esto es un script para instalar el driver privativo de nvidia en Debian o Ubuntu \n(derivadas de Ubuntu también)\n\nPara poder realizar cambios en tu sistema\nnecesitas ejecutar el script como superusuario\nusando \n'sudo sh script.sh'" "15" "35"
    exit
    
else
    
    whiptail --title "Nvidia drivers Installer" \
    --yesno "Esto es un script para instalar el driver privativo de nvidia en Debian o Ubuntu \n(derivadas de Ubuntu también) \n\nDeseas continuar? " 15 35 \
    
    
    
    ans=$?
    # echo $ans
    if [ $ans -eq 0 ]
    then
        
        whiptail --title "Nvidia drivers Installer" \
        --yesno "El script ha detectado que tu distribucion es\n\n$dist \n\nEs esto correcto?" 15 35 \
        # echo "Respondió si"
        ans=$?
        # echo $ans
        if [ $ans -eq 0 ]
        then
            echo 'Driver detectado correcto'
            if [ $distL = $distro1 ]
            then
                echo 'Es Debian!'
                # sudo apt install -y nvidia-detect
                # nvidia-detect
                # driver=$(nvidia-detect | grep "nvidia-" | sed 's/ //g' ) ;
                driver='version.xxx'
                whiptail --title "Nvidia drivers Installer" \
                --yesno "Proceder a instalar $driver?" 15 35 \
                ans=$?
                if [ $ans -eq 0 ]
                then
                    echo 'Install on Debian!'
                    # sudo apt install "$driver"
                    whiptail --title "Nvidia drivers Installer" \
                    --yesno "Para aplicar los cambios\nel sistema necesita reiniciarse\nDeseas hacerlo ahora?" 15 35
                    ans1=$?
                    echo $ans1
                    if [ $ans1 -eq 0 ]
                    then
                    whiptail --msgbox "Gracias por usar este script!" "15" "35"
                        sleep 2
                        #    sudo reboot;
                        echo 'Rebooting!'
                    else
                        whiptail --title "Nvidia drivers Installer" \
                        --msgbox "Sin cambios!" 15 35
                        echo "Respondió no"
                    fi
                else
                    whiptail --msgbox "La operacion fue cancelada" "15" "35"
                    echo 'No'
                fi
                
                
            fi
            if [ $distL = $distro2 ]
            
            then
                # driver='ubuntu.version.xxx'
                # whiptail --title "Nvidia drivers Installer" \
                # --yesno "Proceder a instalar $driver?" 15 35 \
                # ans=$?
                whiptail --title "Nvidia drivers Installer" \
                --yesno "Proceder a instalar el driver generico?" 15 35 \
                # echo "Respondió si"
                ans=$?
                # echo $ans
                if [ $ans -eq 0 ]
                then
                    echo 'Install on Debian!'
                    # sudo ubuntu-drivers autoinstall
                    whiptail --title "Nvidia drivers Installer" \
                    --yesno "Para aplicar los cambios\nel sistema necesita reiniciarse\nDeseas hacerlo ahora?" 15 35
                    ans1=$?
                    echo $ans1
                    if [ $ans1 -eq 0 ]
                    then
                    whiptail --msgbox "Gracias por usar este script!" "15" "35"
                        sleep 2
                        #    sudo reboot;
                        echo 'Rebooting!'
                    else
                        whiptail --title "Nvidia drivers Installer" \
                        --msgbox "Sin cambios!" 15 35
                        echo "Respondió no"
                    fi
                else
                    whiptail --msgbox "La operacion fue cancelada" "15" "35"
                    echo 'No'
                fi
            fi
            
            if ! [ $distL = $distro1 ] && ! [ $distL = $distro2 ]
            
            then
                echo 'Es otra!'
            fi
            
            
        else
            CHOICE=$(whiptail --menu "Cual tu distro Linux?" 18 35 10 \
                "Ubuntu" "o derivadas" \
            "Debian" "Test o estable" 3>&1 1>&2 2>&3)
            
            if [ -z "$CHOICE" ]; then
                echo "No option was chosen (user hit Cancel)"
            else
                
                if [ "$CHOICE" = "Debian" ]; then
                    # sudo apt install -y nvidia-detect
                    # nvidia-detect
                    # driver=$(nvidia-detect | grep "nvidia-" | sed 's/ //g' ) ;
                    driver='version.xxx'
                    echo 'Para Debian!'
                    whiptail --title "Nvidia drivers Installer" \
                    --yesno "Proceder a instalar $driver?" 15 35 \
                    # echo "Respondió si"
                    ans=$?
                    # echo $ans
                    if [ $ans -eq 0 ]
                    then
                        echo 'Install on Debian!'
                        # sudo ubuntu-drivers autoinstall
                        whiptail --title "Nvidia drivers Installer" \
                        --yesno "Para aplicar los cambios\nel sistema necesita reiniciarse\nDeseas hacerlo ahora?" 15 35
                        ans1=$?
                        echo $ans1
                        if [ $ans1 -eq 0 ]
                        then
                        whiptail --msgbox "Gracias por usar este script!" "15" "35"
                        sleep 2
                            #    sudo reboot;
                            echo 'Rebooting!'
                        else
                            whiptail --title "Nvidia drivers Installer" \
                            --msgbox "Sin cambios!" 15 35
                            echo "Respondió no"
                        fi
                    else
                        whiptail --msgbox "La operacion fue cancelada" "15" "35"
                        echo 'No'
                    fi
                    
                else
                    # sudo ubuntu-drivers autoinstall
                    echo 'Para Ubuntu!'
                    whiptail --title "Nvidia drivers Installer" \
                --yesno "Proceder a instalar el driver generico?" 15 35 \
                # echo "Respondió si"
                ans=$?
                # echo $ans
                if [ $ans -eq 0 ]
                then
                    echo 'Install on Debian!'
                    # sudo ubuntu-drivers autoinstall
                    whiptail --title "Nvidia drivers Installer" \
                    --yesno "Para aplicar los cambios\nel sistema necesita reiniciarse\nDeseas hacerlo ahora?" 15 35
                    ans1=$?
                    echo $ans1
                    if [ $ans1 -eq 0 ]
                    then
                    whiptail --msgbox "Gracias por usar este script!" "15" "35"
                        sleep 2
                        #    sudo reboot;
                        echo 'Rebooting!'
                    else
                        whiptail --title "Nvidia drivers Installer" \
                        --msgbox "Sin cambios!" 15 35
                        echo "Respondió no"
                    fi
                else
                    whiptail --msgbox "La operacion fue cancelada" "15" "35"
                    echo 'No'
                fi
                    
                fi
                
                
            fi
            
        fi
        
        
        
        
        
    fi #Fin acepta inicio
    
fi #Fin root analisis
